#!/usr/bin/env ruby
require 'open3'

DEBUG=false

testgroup=[1,10,100,500,800,1000,1100,1500,2000,2500,3000,3500,4000,4500,5000]
target=ARGV[0]
maxcurr=ARGV[1]

target||="http://localhost:3000/1/alfred_manifest/afr-universal/live"
maxcurr||=100
maxcurr=maxcurr.to_i
def start target_url,client=1,num=1
  _cmd="ab -c #{client} -n #{num} -k -r #{target_url}"
  Open3.popen3(_cmd){|stdin,stdout,stderr,thread|
    begin
      puts stderr.read  unless stderr.nil?
      result=stdout.read.lines.inject({cmd:_cmd}){|r,line|
        puts line if DEBUG
        case line
        when /Concurrency*/
          r[:currnum]=line.gsub(/\d.*/).first
        when /Complete*/
          r[:completed]=line.gsub(/\d.*/).first
        when /Failed*/
          r[:failed]=line.gsub(/\d.*/).first
        when /Requests per second*/
          r[:reqps]=line.gsub(/[0-9]*\.[0-9]*/).first
        when /mean,* across*/
          r[:timepr]=line.gsub(/[0-9]*\.[0-9]*/).first
        end
        r
      }
    rescue Exception=>e
      puts "excpetion #{e}"
      p thread
      Process.kill("KILL",thread.pid)
    end
  }
end

result=testgroup.inject([]){|r,clientnum|
  break r if clientnum>maxcurr

  puts "===================================="
  puts ">>>ClientNum:#{clientnum}"
  cur=start(target,clientnum,clientnum)
  puts ">>>Result:#{cur.delete_if{|k,v| k==:cmd}}"
  r<< cur
  sleep 3
  r
}

begin
  puts "**************************************"
  puts result.map{|r|
    r[:completed]=r[:completed].to_i-r[:failed].to_i
    r.delete_if{|k,v| (k==:cmd or k==:failed)}
    r
  }
  puts "Max Curr Level: #{result.max_by{|r| r[:completed].to_i}[:completed]}"
  puts "Request per second : #{(result.reduce(0){|r,i| r+i[:reqps].to_f}.to_f/result.size).round(3)}"
  puts "Time per request: #{(result.reduce(0){|r,i| r+i[:timepr].to_f}.to_f/result.size).round(3)} ms"
  puts "**************************************"
rescue Exception=>e
  p result
  p e.backtrace.inspect
end
puts "DONE"
